/*****************************************************************************
*
*   M�dulo 1 - Unidad 2 - Ejercicios - Hoja 04
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/
USE ciclistas;

-- 1 Nombre y edad de los ciclistas que han ganado etapas.
SELECT DISTINCT c.nombre, c.edad 
  FROM etapa e INNER JOIN ciclista c USING(dorsal);


-- 2 Nombre y edad de los ciclistas que han ganado puertos.
SELECT DISTINCT c.nombre, c.edad 
  FROM puerto p INNER JOIN ciclista c USING(dorsal);
 

-- 3 Nombre y edad de los ciclistas que han ganado etapas Y puertos.
SELECT DISTINCT c.nombre, c.edad 
  FROM 
    etapa e 
    INNER JOIN puerto p USING(dorsal) 
    INNER JOIN ciclista c USING(dorsal);

-- 4 Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa.
SELECT DISTINCT director
  FROM
    etapa e
    INNER JOIN ciclista c USING (dorsal)
    INNER JOIN equipo eq using (nomequipo);

-- 5 Dorsal y nombre de los ciclistas que hayan llevado alg�n maillot
SELECT DISTINCT c.nombre, c.dorsal
  FROM 
    lleva l 
    INNER JOIN ciclista c ON l.dorsal = c.dorsal;


-- 6 Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo
SELECT DISTINCT c.nombre, c.dorsal
  FROM 
    lleva l 
    INNER JOIN ciclista c ON l.dorsal = c.dorsal
    INNER JOIN maillot m ON l.c�digo = m.c�digo
  WHERE m.color = "AMARILLO";


-- 7 Dorsal de los ciclistas que hayan llevado alg�n maillot y que han ganado etapas.
SELECT DISTINCT e.dorsal
  FROM lleva ll INNER JOIN etapa e ON ll.dorsal = e.dorsal;


-- 8 Indicar el numetapa de las etapas que tengan puertos.
SELECT DISTINCT numetapa FROM puerto;
 
-- 9 Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos.
SELECT kms, e.numetapa FROM etapa e INNER JOIN ciclista c ON e.dorsal = c.dorsal WHERE c.nomequipo = "BANESTO";

SELECT etbanesto.kms,
       etbanesto.numetapa
  FROM 
    (SELECT kms, e.numetapa FROM etapa e INNER JOIN ciclista c ON e.dorsal = c.dorsal WHERE c.nomequipo = "BANESTO") etbanesto
    INNER JOIN 
    (SELECT DISTINCT numetapa FROM puerto) etpuertos
    USING ( numetapa );


-- 10 Listar el n�mero de ciclistas que hayan ganado alguna etapa con puerto
SELECT DISTINCT c.dorsal FROM ciclista c INNER JOIN puerto p ON c.dorsal = p.dorsal;

-- 11 Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto
SELECT DISTINCT p.nompuerto FROM ciclista c INNER JOIN puerto p ON c.dorsal = p.dorsal WHERE c.nomequipo = "BANESTO";

-- 12.1 Listar el n�mero de etapas que tengan puerto que hayan sido ganados (las etapas) por ciclistas de Banesto, con mas de 200 km.
SELECT COUNT(DISTINCT e.numetapa)
  FROM 
    etapa e
    INNER JOIN ciclista c ON e.dorsal = c.dorsal
    INNER JOIN puerto p ON e.numetapa = p.numetapa
  WHERE 
    c.nomequipo = "BANESTO"
    AND
    e.kms > 100;

-- 12.2 Listar el n�mero de etapas que tengan puerto que hayan sido ganados (los puertos) por ciclistas de Banesto, con mas de 200 km.
SELECT COUNT(DISTINCT e.numetapa)
  FROM 
    etapa e
    INNER JOIN puerto p ON e.numetapa = p.numetapa
    INNER JOIN ciclista c ON p.dorsal = c.dorsal
  WHERE 
    c.nomequipo = "BANESTO"
    AND
    e.kms > 100;