/*****************************************************************************
*
*   M�dulo 1 - Unidad 2 - Ejercicios - Hoja 07
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/

SET NAMES 'utf8';
USE m1u2h7;



-- Ejercicio 1

-- SELECT p.`C�DIGO ART�CULO`, p.SECCI�N, p.PRECIO FROM productos p WHERE p.precio > 100;

CREATE OR REPLACE VIEW CONSULTA1 AS 
  SELECT p.`C�DIGO ART�CULO`, p.SECCI�N, p.PRECIO 
    FROM productos p 
    WHERE p.precio > 100
;

SHOW FULL TABLES;
SELECT * FROM CONSULTA1;



-- Ejercicio 2

-- SELECT p.`C�DIGO ART�CULO`, p.SECCI�N, p.PRECIO FROM productos p WHERE p.precio > 100 ORDER BY precio DESC;

CREATE OR REPLACE VIEW CONSULTA1 AS 
  SELECT p.`C�DIGO ART�CULO`, p.SECCI�N, p.PRECIO 
    FROM productos p 
    WHERE p.precio > 100
    ORDER BY precio DESC
-- WITH CHECK OPTION
-- WITH LOCAL CHECK OPTION
-- WITH CASCADED CHECK OPTION
;

SHOW FULL TABLES;
SELECT * FROM CONSULTA1;



-- Ejercicio 3

-- SELECT * FROM CONSULTA1 p WHERE p.`SECCI�N` = "DEPORTES";

CREATE OR REPLACE VIEW CONSULTA2 AS 
  SELECT * 
    FROM CONSULTA1 
    WHERE `SECCI�N` = "DEPORTES" 
    ORDER BY precio DESC;
-- Al filtrar subvistas, NO TE GARANTIZAN EL ORDEN: reordena tambien en las subvistas

-- Ejercicio 4
SHOW FULL TABLES;
SELECT * FROM CONSULTA2;


-- Ejercicio 5
DELETE FROM productos WHERE `c�digo art�culo` = "AR90";
INSERT INTO `CONSULTA1` (`c�digo art�culo`, `secci�n`, `precio`) VALUE ("AR90", "NOVEDADES", 5);
SELECT * FROM productos WHERE `c�digo art�culo` = "AR90";
-- SE INSERT� a pesar de que no cumple las condiciones para poder ser visible en la vista (precio<100).

-- Dej� insertar porque no hay ningun check puesto. Si lo estuviesen, las condiciones del WHERE
-- serian requisito para que se ejecutase (aparte de las t�picas como que la clave primaria no 
-- exista ya, etc.).
-- LOCAL CHECK solo comprueban los requisitos de dicha vista.
-- CASCADED CHECK comprueba las de dicha vista y las subvistas heredadas.


-- Ejercicio 6 
-- ALTER VIEW CONSULTA1 AS WITH local CHECK OPTION;
CREATE OR REPLACE VIEW CONSULTA1 AS 
  SELECT p.`C�DIGO ART�CULO`, p.SECCI�N, p.PRECIO 
    FROM productos p 
    WHERE p.precio > 100
    ORDER BY precio DESC
  WITH LOCAL CHECK OPTION
;

-- Ejercicio 7
DELETE FROM productos WHERE `c�digo art�culo` = "AR91";
INSERT INTO `CONSULTA1` (`c�digo art�culo`, `secci�n`, `precio`) VALUE ("AR91", "NOVEDADES", 5);
SELECT * FROM productos WHERE `c�digo art�culo` = "AR91";

SELECT * FROM productos WHERE SECCI�N = "NOVEDADES";

-- NO SE INSERT� porque no cumple las condiciones para poder ser visible en la vista (precio<100).
-- Si fuese una subvista de otra, con LOCAL CHECK solo comprueba las condiciones de la vista 
-- llamada, no las de las previas.
INSERT INTO `CONSULTA1` (`c�digo art�culo`, `secci�n`, `precio`) VALUE ("AR91", "NOVEDADES", 110);
SELECT * FROM productos WHERE `c�digo art�culo` = "AR91";

SELECT * FROM productos WHERE SECCI�N = "NOVEDADES";



-- Ejercicio 8
DELETE FROM productos WHERE `c�digo art�culo` = "AR92";
INSERT INTO `CONSULTA2` (`c�digo art�culo`, `secci�n`, `precio`) VALUE ("AR92", "NOVEDADES", 5);
SELECT * FROM productos WHERE `c�digo art�culo` = "AR92";
-- Funciona porque no hace check de ningun tipo



-- Ejercicio 9
CREATE OR REPLACE VIEW CONSULTA2 AS 
  SELECT * 
    FROM CONSULTA1 
    WHERE `SECCI�N` = "DEPORTES" 
    ORDER BY precio DESC
  WITH LOCAL CHECK OPTION;

-- Ejercicio 10
DELETE FROM productos WHERE `c�digo art�culo` = "AR93";
INSERT INTO `CONSULTA2` (`c�digo art�culo`, `secci�n`, `precio`) VALUE ("AR93", "NOVEDADES", 5);
SELECT * FROM productos WHERE `c�digo art�culo` = "AR93";
-- Falla la inserci�n porque no pertenece a la seccion de  deportes

DELETE FROM productos WHERE `c�digo art�culo` = "AR93";
INSERT INTO `CONSULTA2` (`c�digo art�culo`, `secci�n`, `precio`) VALUE ("AR93", "DEPORTES", 5);
SELECT * FROM productos WHERE `c�digo art�culo` = "AR93";
-- La insercion funciona porque cumple que es de la seccion de deportes, aunque deber�a haber
 -- fallado, porque CONSULTA1 requiere que el precio sea m�s de 100.


-- Ejercicio 11
CREATE OR REPLACE VIEW CONSULTA2 AS 
  SELECT * 
    FROM CONSULTA1 
    WHERE `SECCI�N` = "DEPORTES" 
    ORDER BY precio DESC
  WITH CASCADED CHECK OPTION;

-- Ejercicio 12
DELETE FROM productos WHERE `c�digo art�culo` = "AR94";
INSERT INTO `CONSULTA2` (`c�digo art�culo`, `secci�n`, `precio`) VALUE ("AR94", "DEPORTES", 5);
SELECT * FROM productos WHERE `c�digo art�culo` = "AR94";
-- La insercion falla, porque aunque cumple que es de la seccion de deportes (CONSULTA2), el 
-- precio no supera los 100 (CONSULTA1) porque CONSULTA1 requiere que el precio sea m�s de 100.
INSERT INTO `CONSULTA2` (`c�digo art�culo`, `secci�n`, `precio`) VALUE ("AR94", "DEPORTES", 200);
SELECT * FROM productos WHERE `c�digo art�culo` = "AR94";


SELECT * FROM productos WHERE `C�DIGO ART�CULO` LIKE "AR9%";

