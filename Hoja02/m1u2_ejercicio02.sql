/*****************************************************************************
*
*   M�dulo 1 - Unidad 2 - Ejercicios - Hoja 02
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/

USE ciclistas;

/*
 * Clausualas de totales
 */


-- 1 Numero de ciclistas que hay
SELECT COUNT(*) FROM ciclista c;

-- 2 Numero de ciclistas que hay del equipo Banesto
SELECT COUNT(*) FROM ciclista c WHERE c.nomequipo = "BANESTO";

-- 3 La edad media de los ciclistas 
SELECT AVG(c.edad) AS edadMedia FROM ciclista c;
SELECT (SUM(c.edad) / COUNT(c.edad)) AS edadMedia FROM ciclista c;

-- 4 La edad media de los de equipo Banesto
SELECT AVG(c.edad) AS edadMedia FROM ciclista c  WHERE c.nomequipo = "BANESTO";
SELECT (SUM(c.edad) / COUNT(c.edad)) AS edadMedia FROM ciclista c WHERE c.nomequipo = "BANESTO";

-- 5 La edad media de los ciclistas por cada equipo 
SELECT c.nomequipo, AVG(c.edad) AS edadMedia 
  FROM ciclista c  
  GROUP BY c.nomequipo;
SELECT c.nomequipo, (SUM(c.edad) / COUNT(c.edad)) AS edadMedia
  FROM ciclista c  
  GROUP BY c.nomequipo;

-- 6 El n�mero de ciclistas por equipo 
SELECT c.nomequipo, COUNT(*) 
  FROM ciclista c 
  GROUP BY c.nomequipo;

-- 7 El n�mero total de puertos
SELECT COUNT(*) 
  FROM puerto p;

-- 8 El n�mero total de puertos mayores de 1500
SELECT COUNT(*) 
  FROM puerto p 
  WHERE p.altura>1500;

-- 9 Listar el nombre de los equipos que tengan m�s de 4 ciclistas 
SELECT c.nomequipo
  FROM ciclista c 
  GROUP BY c.nomequipo
  HAVING COUNT(*) > 4
;

SELECT equipos.nomequipo 
  FROM (
    SELECT c.nomequipo, COUNT(*) numciclistas
      FROM ciclista c 
      GROUP BY c.nomequipo
  ) equipos
  WHERE equipos.numciclistas > 4
;

-- 10 Listar el nombre de los equipos que tengan m�s de 4 ciclistas cuya edad este entre 28 y 32 
SELECT c.nomequipo
  FROM ciclista c
  WHERE c.edad BETWEEN 28 AND 32
  GROUP BY nomequipo
  HAVING COUNT(*) > 4
;

-- 11 Ind�came el n�mero de etapas que ha ganado cada uno de los ciclistas 
SELECT e.dorsal , COUNT(*) victorias
  FROM etapa e
  GROUP BY e.dorsal;

SELECT c.nombre, etapas.victorias
  FROM ciclista c INNER JOIN (
    SELECT e.dorsal , COUNT(*) victorias
      FROM etapa e
      GROUP BY e.dorsal
  ) etapas
  ON c.dorsal = etapas.dorsal;
-- �
SELECT c.nombre, COUNT(*) victorias
  FROM ciclista c INNER JOIN etapa e USING (dorsal)
  GROUP BY c.nombre;


-- 12 Ind�came el dorsal de los ciclistas que hayan ganado m�s de 1 etapa 
SELECT e.dorsal , COUNT(*) victorias
  FROM etapa e
  GROUP BY e.dorsal
  HAVING victorias>1;
