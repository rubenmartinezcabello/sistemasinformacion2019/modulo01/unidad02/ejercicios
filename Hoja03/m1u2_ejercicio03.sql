/*****************************************************************************
*
*   M�dulo 1 - Unidad 2 - Ejercicios - Hoja 03
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/

USE ciclistas;

/*
 * Clausualas de totales
 */


--  1 Listar las edades de todos los ciclistas de Banesto
SELECT DISTINCT c.edad 
  FROM ciclista c 
  WHERE c.nomequipo = "BANESTO";
--
SELECT DISTINCT c.edad 
  FROM ciclista c 
  WHERE c.nomequipo LIKE "BANESTO";



--  2 Listar las edades de los ciclistas que son de Banesto o de Navigare
SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo = "BANESTO"
UNION
SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo = "NAVIGARE";
-- Al hacer UNION, mientras no hagamos UNION ALL (que equivale a una concatenacion simple), 
-- se hace un distinct implicito porque en (union de) conjuntos no existe la cardinalidad del elemento.
SELECT c.edad FROM ciclista c WHERE c.nomequipo = "BANESTO"
UNION
SELECT c.edad FROM ciclista c WHERE c.nomequipo = "NAVIGARE";


SELECT DISTINCT c.edad 
  FROM ciclista c 
  WHERE c.nomequipo = "BANESTO" OR c.nomequipo = "NAVIGARE";



--  3 Listar el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32
SELECT c.nombre FROM ciclista c WHERE c.nomequipo = "BANESTO"
INTERSECT
SELECT c.nombre FROM ciclista c WHERE c.edad BETWEEN 25 AND 32;
--
SELECT c.nombre FROM ciclista c WHERE c.nomequipo = "BANESTO" AND c.edad BETWEEN 25 AND 32;



--  4 Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32
SELECT c.nombre FROM ciclista c WHERE c.nomequipo = "BANESTO"
UNION
SELECT c.nombre FROM ciclista c WHERE c.edad BETWEEN 25 AND 32;
--
SELECT c.nombre FROM ciclista c WHERE c.nomequipo = "BANESTO" OR c.edad BETWEEN 25 AND 32;



--  5 Listar la inicial del equipo de los ciclistas cuyo nombre comience por R
SELECT LEFT(c.nomequipo, 1)
  FROM ciclista c 
  WHERE c.nombre LIKE "r%";



--  6 Listar el c�digo de las etapas que su salida y llegada sea en la misma poblaci�n.
SELECT e.numetapa
  FROM etapa e
  WHERE e.salida = e.llegada;


--  7 Listar el c�digo de las etapas que su salida y llegada no sean en la misma poblaci�n y que conozcamos el dorsal del ciclista que ha ganado la etapa.
SELECT e.numetapa, dorsal
  FROM etapa e
  WHERE 
    e.salida <> e.llegada
    AND
    dorsal IS NOT NULL
    ;



--  8 Listar el nombre de los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400.
SELECT p.nompuerto
  FROM puerto p 
  WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura>2400;



--  9 Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400.
SELECT DISTINCT p.dorsal
  FROM puerto p 
  WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura>2400;



-- 10 Listar el n�mero de ciclistas que hayan ganado alguna etapa.
SELECT COUNT(DISTINCT e.dorsal) FROM etapa e;
--
SELECT COUNT(*) FROM (
  SELECT DISTINCT e.dorsal FROM etapa e
) ganadores;



-- 11 Listar el n�mero de etapas que tengan puerto.
SELECT COUNT(DISTINCT p.numetapa) FROM puerto p;



-- 12 Listar el n�mero de ciclistas que hayan ganado alg�n puerto.
SELECT COUNT(DISTINCT p.dorsal) FROM puerto p;



-- 13 Listar el c�digo de la etapa con el n�mero de puertos que tiene.
SELECT p.numetapa, COUNT(*) numpuertos FROM puerto p GROUP BY p.numetapa;



-- 14 Indicar la altura media de los puertos.
SELECT AVG(p.altura) FROM puerto p;



-- 15 Indicar el c�digo de etapa cuya altura media de sus puertos est� por encima de 1500.
SELECT p.numetapa 
  FROM  puerto p 
  GROUP BY p.numetapa 
  HAVING AVG(p.altura)>1500;



-- 16 Indicar el n�mero de etapas que cumplen la condici�n anterior.
SELECT COUNT(*) 
  FROM 
  (
    SELECT p.numetapa
    FROM  puerto p 
    GROUP BY p.numetapa 
    HAVING AVG(p.altura) > 1500
  ) etapas;




-- 17 Listar el dorsal del ciclista con el n�mero de veces que ha llevado alg�n maillot.
SELECT ll.dorsal, COUNT(*) FROM lleva ll GROUP BY ll.dorsal;



-- 18 Listar el dorsal del ciclista con el c�digo de maillot y cuantas veces ese ciclista ha llevado ese maillot.
SELECT ll.dorsal, ll.c�digo, COUNT(*) FROM lleva ll GROUP BY ll.dorsal, ll.c�digo;


-- 19 Listar el dorsal el c�digo de etapa, el ciclista y el n�mero de maillots que ese ciclista a llevado en cada etapa.
SELECT ll.dorsal, ll.numetapa, COUNT(*) FROM lleva ll GROUP BY ll.dorsal, ll.numetapa;
