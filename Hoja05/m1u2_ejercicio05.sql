/*****************************************************************************
*
*   M�dulo 1 - Unidad 2 - Ejercicios - Hoja 05
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/
USE ciclistas;

-- 1 Nombre y edad de los ciclistas que NO han ganado etapas 
SELECT DISTINCT c.dorsal AS noGanaEtapas, c.nombre, c.edad
  FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal
  WHERE e.dorsal IS NULL;


-- 2 Nombre y edad de los ciclistas que NO han ganado puertos
SELECT DISTINCT c.dorsal AS noGanaPuertos, c.nombre, c.edad
  FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal
  WHERE p.dorsal IS NULL;


-- 3 Listar el director de los equipos que tengan ciclistas NO que hayan ganado NINGUNA etapa

-- 3.1 Ciclistas que han ganado alguna etapa
SELECT DISTINCT dorsal FROM etapa;

-- 3.2 Equipos
SELECT DISTINCT nomequipo FROM equipo e;

-- 3.3 Equipos que han ganado alguna etapa
SELECT DISTINCT nomequipo FROM ciclista c INNER JOIN (SELECT DISTINCT dorsal FROM etapa) ganadores ON c.dorsal=ganadores.dorsal;

-- 3.4 Equipos que NO han ganado NINGUNA etapa = NEG (equipos que HAN ganado ALGUNA etapa)
SELECT * 
  FROM 
    (SELECT DISTINCT nomequipo FROM equipo e) equipos 
    LEFT OUTER JOIN
    (SELECT DISTINCT nomequipo FROM ciclista c INNER JOIN (SELECT DISTINCT dorsal FROM etapa) ganadores ON c.dorsal=ganadores.dorsal) equiposganadores
    USING(nomequipo) -- ON equipos.nomequipo = equiposganadores.nomequipo
  WHERE equiposganadores.nomequipo IS NULL;


-- 4 Dorsal y nombre de los ciclistas que NO hayan llevado alg�n maillot

-- 4.1 Ciclistas que han llevado algun maillot
SELECT DISTINCT ll.dorsal 
  FROM lleva ll;

-- 4.2 Ciclistas que no han llevado algun maillot
SELECT DISTINCT c.dorsal 
  FROM ciclista c LEFT OUTER JOIN lleva ll ON c.dorsal = ll.dorsal;

-- 4.3 Ciclistas que no han llevado algun maillot (optimizado)
SELECT c.dorsal, c.nombre 
  FROM ciclista c LEFT OUTER JOIN (SELECT DISTINCT dorsal FROM lleva) ll ON c.dorsal = ll.dorsal WHERE ll.dorsal IS NULL;


-- 5 Dorsal y nombre de los ciclistas que NO hayan llevado el maillot amarillo NUNCA 

-- 5.1 Dorsal de los ciclistas que HAYAN llevado el maillot amarillo ALGUNA vez
SELECT DISTINCT ll.dorsal 
  FROM lleva ll INNER JOIN maillot m ON ll.c�digo = m.c�digo
  WHERE m.color = "AMARILLO";

-- 5.2 Ciclistas que no han llevado el maillot amarillo alguna vez
SELECT c.dorsal, c.nombre  
  FROM 
    ciclista c 
    LEFT OUTER JOIN 
    (
      SELECT DISTINCT ll.dorsal 
        FROM lleva ll INNER JOIN maillot m ON ll.c�digo = m.c�digo
        WHERE m.color = "AMARILLO"
    ) a 
    USING(dorsal)
  WHERE a.dorsal IS NULL;


-- 6 Indicar el numetapa de las etapas que NO tengan puertos

-- 6.1 Etapas con puertos 
SELECT DISTINCT numetapa FROM puerto p;

-- 6.2 Etapas sin puertos
SELECT numetapa FROM etapa e LEFT OUTER JOIN (SELECT DISTINCT numetapa FROM puerto p) p USING(numetapa) WHERE p.numetapa IS NULL;


-- 7 Indicar la distancia media de las etapas que NO tengan puertos 
SELECT AVG(kms) AS distanciamedia FROM etapa e LEFT OUTER JOIN (SELECT DISTINCT numetapa FROM puerto p) p USING(numetapa) WHERE p.numetapa IS NULL;


-- 8 Listar el n�mero de ciclistas que NO hayan ganado alguna etapa 
SELECT COUNT(DISTINCT c.dorsal)
  FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal
  WHERE e.dorsal IS NULL;


-- 9 Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto 

-- 9.1 Etapas sin puertos (reuse 6.2)
SELECT numetapa FROM etapa e LEFT OUTER JOIN (SELECT DISTINCT numetapa FROM puerto p) p USING(numetapa) WHERE p.numetapa IS NULL;

-- 9.2 ganadores de etapas sin puertos
SELECT DISTINCT e.dorsal
  FROM etapa e INNER JOIN (SELECT numetapa FROM etapa e LEFT OUTER JOIN (SELECT DISTINCT numetapa FROM puerto p) p USING(numetapa) WHERE p.numetapa IS NULL) esinp
  ON e.numetapa = esinp.numetapa;


-- 10 Listar el dorsal de los ciclistas que hayan ganado �nicamente etapas que no tengan puertos (ganadoresDeEtapas - ganadoresDeEtapasConPuertos)
-- 10.1 Ganadores de etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- 10.2 Etapas con puertos
SELECT DISTINCT p.numetapa FROM puerto p;

-- 10.3 Dorsales de ganadores de etapas con puertos
-- MAL: salen ganadores de puertos, no de etapas:
-- SELECT DISTINCT c.dorsal FROM puerto p INNER JOIN ciclista c ON p.dorsal = c.dorsal;

-- 10.4 Ganadores de etapas con puerto (no ganadores de puertos)
SELECT DISTINCT dorsal
  FROM 
    (SELECT DISTINCT p.numetapa FROM puerto p) epuertos
    INNER JOIN
    etapa e ON epuertos.numetapa = e.numetapa
;

-- 10.5 Ganadores de etapas que no hayan ganado etapas con puertos
SELECT ganadores.dorsal
  FROM
    ( SELECT DISTINCT e.dorsal FROM etapa e ) ganadores
    LEFT OUTER JOIN
    ( SELECT DISTINCT dorsal FROM (SELECT DISTINCT p.numetapa FROM puerto p) epuertos INNER JOIN etapa e ON epuertos.numetapa = e.numetapa ) ganadoreDePuertos
    ON ganadores.dorsal = ganadoreDePuertos.dorsal
  WHERE ganadoreDePuertos.dorsal IS NULL
;
